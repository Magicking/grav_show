# flow3r imports
from st3m import InputState, run
from st3m.application import Application, ApplicationContext
from ctx import Context
from math import cos, sin
from st3m.utils import tau
import leds

class GravShow(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        factor = 1/2
        self.pt = {}
        self.pt[0] = [0, 0, 0]
        self.pt[1] = [120*factor, 0, 0]

        self.pt[2] = [0, 0, 0]
        self.pt[3] = [0, 120*factor, 0]

        self.pt[4] = [0, 0, 0]
        self.pt[5] = [0, 0, 120*factor]
        self._angle = 0.0
        self.matrix = self.orthographic_projection_matrix()
        self._x = 0
        self._y = 0
        self._z = 0

        #self._x = -20

    def get_rotation_matrix_from_gyro(self, gyro_data, delta_time):
        # Compute the rotation angles for each axis
        # Angular velocity * time = angle of rotation
        roll_angle = gyro_data[0] * delta_time
        pitch_angle = gyro_data[1] * delta_time
        yaw_angle = gyro_data[2] * delta_time
    
        # Compute rotation matrices for each axis based on angles
        # These are the same rotation matrices as described before.
        roll_matrix = [
            [1, 0, 0],
            [0, cos(roll_angle), -sin(roll_angle)],
            [0, sin(roll_angle), cos(roll_angle)]
        ]
    
        pitch_matrix = [
            [cos(pitch_angle), 0, sin(pitch_angle)],
            [0, 1, 0],
            [-sin(pitch_angle), 0, cos(pitch_angle)]
        ]
    
        yaw_matrix = [
            [cos(yaw_angle), -sin(yaw_angle), 0],
            [sin(yaw_angle), cos(yaw_angle), 0],
            [0, 0, 1]
        ]
    
        # Combine the rotation matrices. The order can affect the final result.
        # Here we're applying yaw, then pitch, then roll.
        combined_matrix = self.matrix_mult(yaw_matrix, self.matrix_mult(pitch_matrix, roll_matrix))
    
        return combined_matrix

    def draw(self, ctx: Context) -> None:
        matrix = self.matrix

        # clean the screen
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.rgb(1, 0, 0)
        self.draw_3d_line(ctx, self.pt[0], self.pt[1])

        ctx.rgb(0, 1, 0)
        self.draw_3d_line(ctx, self.pt[2], self.pt[3])

        ctx.rgb(0, 0, 1)
        self.draw_3d_line(ctx, self.pt[4], self.pt[5])

        ctx.rgb(1, 1, 0)
        zpt = (0, 0, 0)
        self.draw_3d_line(ctx, zpt, (self._x, 0, 0))
        ctx.rgb(0, 1, 1)
        self.draw_3d_line(ctx, zpt, (0, self._y, 0))
        ctx.rgb(1, 0, 1)
        self.draw_3d_line(ctx, zpt, (0, 0, self._z))

    ## Determine the direction of the accelerometer
    ## If the accelerometer is pointing up, it will draw image 1
    ## If the accelerometer is pointing down, it will draw image 2
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        #if ins.imu.acc[0] > 0o:
        # Rotate the matrix by the angle
        (x, y, z) = ins.imu.gyro
        # if gyro has a too low value zero it
        if abs(x) < 1:
            x = 0
        if abs(y) < 1:
            y = 0
        if abs(z) < 1:
            z = 0

        self._x = x
        self._y = y
        self._z = z

        gyro_matrix = self.get_rotation_matrix_from_gyro((x, y, z), delta_ms / 10000.0)
        for i in range(6):
            self.pt[i] = self.matrix_vector_mult(gyro_matrix, self.pt[i])
        print("Matrix: " + str(ins.imu.gyro))
        print("Matrix: " + str(gyro_matrix))

    def orthographic_projection_matrix(self):
        """
        Create a 3x3 orthographic projection matrix.
        This will essentially drop the z-coordinate.
        """
        return [
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 0]
        ]
    
    def matrix_mult(self, A, B):
        result = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        for i in range(3):
            for j in range(3):
                for k in range(3):
                    result[i][j] += A[i][k] * B[k][j]
        return result
    def matrix_vector_mult(self, matrix, vector):
        """
        Multiply a 3x3 matrix by a 3D vector.
        """
        result = [0, 0, 0]
        
        for i in range(3):
            for j in range(3):
                result[i] += matrix[i][j] * vector[j]
        
        return result
    
    def project_3d_to_2d(self, point, matrix):
        """
        Project a 3D point to 2D using an orthographic matrix.
        """

        projected = self.matrix_vector_mult(matrix, point)
        return (projected[0], projected[1])
    
    def draw_3d_line(self, ctx: Context, start_3d, end_3d):
        """
        Draw a 3D line using a 2D line primitive function.
        """
        matrix = self.orthographic_projection_matrix()
        start_2d = self.project_3d_to_2d(start_3d, matrix)
        end_2d = self.project_3d_to_2d(end_3d, matrix)
        
        ctx.move_to(*start_2d).line_to(*end_2d).stroke().fill()
        # Pretty print the start and end
        print("Start: " + str(start_2d))
        print("End: " + str(end_2d))
        print("")

if __name__ == '__main__':
    run.run_view(GravShow(ApplicationContext()))
